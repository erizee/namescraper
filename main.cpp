#include <iostream>
#include <string>
#include <curl/curl.h>
#include <libxml/HTMLParser.h>
#include <libxml/xpath.h>
#include <clocale>
#include <algorithm>
#include "utilities.h"


size_t WriteCallback(void* contents, size_t size, size_t nmemb, std::string* output)
{
    size_t totalSize = size * nmemb;
    output->append((char*)contents, totalSize);
    return totalSize;
}

int main()
{
    setlocale( LC_ALL, "Polish" );
    std::string searchedName;
    std::cout<<"Witaj w programie do zbierania informacji o polskich nazwiskach\nPodaj nazwisko do sprawdzenia: ";

    std::cin>>searchedName;
    std::transform(searchedName.begin(), searchedName.end(), searchedName.begin(), ::toupper);

    CURL* curl;
    CURLcode res;
    std::string htmlContent;

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    std::string baseLink = "https://polskienazwiska.pl/n/";
    std::string urlName = baseLink+searchedName;

    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, urlName.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &htmlContent);

        res = curl_easy_perform(curl);

        if (res != CURLE_OK)
        {
            std::cerr << "Failed to fetch webpage: " << curl_easy_strerror(res) << std::endl;
            curl_easy_cleanup(curl);
            curl_global_cleanup();
            return 1;
        }
        curl_easy_cleanup(curl);
    }
    else
    {
        std::cerr << "Failed to initialize CURL." << std::endl;
        curl_global_cleanup();
        return 1;
    }

    xmlInitParser();
    htmlDocPtr doc = htmlReadMemory(htmlContent.c_str(), htmlContent.length(), "", nullptr,
                                    HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);

    if (doc == nullptr)
    {
        std::cerr << "Failed to parse HTML." << std::endl;
        xmlCleanupParser();
        return 1;
    }

    xmlXPathContextPtr xpathContext = xmlXPathNewContext(doc);
    xmlXPathObjectPtr xpathObject = xmlXPathEvalExpression((const xmlChar*)"//div[@id='surname_summary']//text()",
                                                           xpathContext);

    if (xpathObject == nullptr)
    {
        std::cerr << "Failed to evaluate XPath expression." << std::endl;
        xmlXPathFreeContext(xpathContext);
        xmlFreeDoc(doc);
        xmlCleanupParser();
        return 1;
    }

    if(xmlXPathNodeSetIsEmpty(xpathObject->nodesetval)) {
        xmlXPathFreeObject(xpathObject);
        printf("Nie ma takiego nazwiska\n");
        return 1;
    }

    std::string extractedText;
    extractText(xpathObject->nodesetval->nodeTab[0], extractedText);

    std::string toPrint = processText(extractedText);
    std::wprintf(s2ws(toPrint).c_str());

    xmlXPathFreeObject(xpathObject);
    xmlXPathFreeContext(xpathContext);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    curl_global_cleanup();

    return 0;
}

