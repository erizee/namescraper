#ifndef NAMESCRAPER_UTILITIES_H
#define NAMESCRAPER_UTILITIES_H


std::wstring s2ws(const std::string& str)
{
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo( size_needed, 0 );
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

void extractText(xmlNodePtr node, std::string& text)
{
    if (node == nullptr)
        return;

    if (node->type == XML_TEXT_NODE)
    {
        xmlChar* content = xmlNodeGetContent(node);
        if (content != nullptr)
        {
            text.append((const char*)content);
            xmlFree(content);
        }
    }

    extractText(node->children, text);
    extractText(node->next, text);
}


std::string processText(std::string text) {
    std::string::iterator new_end = std::unique(text.begin(), text.end(),
                                                BothAreSpaces);
    text.erase(new_end, text.end());
    std::size_t found = text.find("Polskienazwiska.pl")+19;

    return text.substr(found);
}

#endif //NAMESCRAPER_UTILITIES_H

